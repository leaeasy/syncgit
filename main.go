package main

import (
	"encoding/json"
	"fmt"
	"os"
	"os/exec"
	"path"
	"sync"
)

type SyncSettings struct {
	Cache string
	Syncs []GitSync
}

type GitSync struct {
	Name   string
	Source string
	Target string
}

func CheckEnv(env string){
	config := os.Getenv("CONFIG")
	if(len(config) == 0){
		fmt.Println("Env CONFIG not set, closing")
		os.Exit(1)
	}
}

func ChangeDir(dir string){
	err := os.Chdir(dir)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error changing to directory: %s, exiting.\n", dir)
		os.Exit(1)
	}
}

func ParseConfig() (SyncSettings, error) {
	config := os.Getenv("CONFIG")
	ConfigFile, err := os.Open(config)
	settings := new(SyncSettings)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error while opening config.json: %s.\n", err)
		os.Exit(1)
	}

	err = json.NewDecoder(ConfigFile).Decode(&settings)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error while parsing config.json: %s.\n", err)
		os.Exit(1)
	}

	fmt.Println("Successfully imported Settings from config.json")
	fmt.Printf("Detected %d repos to sync\n", len(settings.Syncs))
	ConfigFile.Close()
	return *settings, err
}

func PullMirror(url string, targetDir string) {
	fmt.Printf("Cloning from %s\n", url)
	cmdName := "git"
	cmdArgs := []string{"clone", "--mirror", url, targetDir}
	cmd := exec.Command(cmdName, cmdArgs...)
	cmdOut, err := cmd.CombinedOutput()
	if err != nil {
		fmt.Fprintf(os.Stderr, "There was an error running git clone command, error %s, stout %s\n", err, string(cmdOut))
	}
}

func FetchMirror(url string, targetDir string){
	fmt.Printf("Fetching updates from %s\n", url)
	ChangeDir(targetDir)
	cmdName := "git"
	cmdArgs := []string{"fetch", "origin", "+refs/heads/*:refs/heads/*", "--prune", "--tags"}
	cmd := exec.Command(cmdName, cmdArgs...)
	cmdOut, err := cmd.CombinedOutput()
	if err != nil {
		fmt.Fprintf(os.Stderr, "There was an error running git clone command, error %s, stout %s\n", err, string(cmdOut))
	}
}

func PushMirror(url string) {
	fmt.Printf("Pushing to %s\n", url)
	cmdName := "git"
	cmdArgs := []string{"push", "--mirror", url}
	cmd := exec.Command(cmdName, cmdArgs...)
	cmdOut, err := cmd.CombinedOutput()
	if err != nil {
		fmt.Fprintf(os.Stderr, "There was an error running git push command, error %s, stout %s\n", err, string(cmdOut))
	}
}

func SyncRepo(cache string, index int, element GitSync, wg *sync.WaitGroup) {
	defer wg.Done()
	fmt.Printf("\n-> Start mirroring %s to %s\n", element.Source, element.Target)

	target := path.Join(cache, element.Name)

	if _, err := os.Stat(target); os.IsNotExist(err) {
		PullMirror(element.Source, target)
	} else {
		FetchMirror(element.Source, target)
	}
	ChangeDir(target)
	PushMirror(element.Target)
	fmt.Printf("\n-> Finished mirroring %s to %s\n", element.Source, element.Target)
}

func main() {
	CheckEnv("CONFIG");
	defer fmt.Println("Syncgit is done.")
	settings, _ := ParseConfig()
	fmt.Printf("Cached directory: %s repos to sync\n", settings.Cache)
	if _, err := os.Stat(settings.Cache); os.IsNotExist(err) {
		fmt.Printf("Create cache directory: %s \n", settings.Cache)
		err := os.MkdirAll(settings.Cache, 0755)
		if err != nil {
			fmt.Fprintf(os.Stderr, "create cache directory error\n")
			os.Exit(1)
		}
	}
	var wg sync.WaitGroup
	wg.Add(len(settings.Syncs))
	for index, sync := range settings.Syncs {
			go SyncRepo(settings.Cache, index, sync, &wg)
	}
	wg.Wait()
}
