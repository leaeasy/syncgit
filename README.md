# syncgit
Parallelized synchronization of git repositories, written in go

## Usage

- Change target and source repos in config.json
- CONFIG=config.json syncgit
